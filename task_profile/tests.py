from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, exp

# Create your tests here.
class tugasProfileUnitTest(TestCase):
	#URL nya ada
    def test_profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)

	#Fungsi index di views.py ada
    def test_profile_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)
        
    #Bio_dict bukan none object
    def test_profile_bio_dict(self):
        self.assertIsNotNone(exp)