from django.shortcuts import render
from task_friend.models import Friends
from task_update.models import Status
# Create your views here.
response= {}
def index(request):
	response['title'] = 'Statistic'
	html = 'task_dashboard/task_dashboard.html'
	response['friends_count'] = Friends.objects.count()
	response['feed_count'] = Status.objects.count()
	if (Status.objects.count() > 0):
		response['latest_post'] = Status.objects.last()
	response['author'] = 'Windi Chandra'
	return render(request, html, response)