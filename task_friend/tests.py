from .models import Friends
from .forms import Friend_Form
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, add_friend
import unittest
# Create your tests here.

class FriendUnitTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/friend/')
        self.assertEqual(response.status_code, 200)

    def test_model_can_add_new_friend(self):
        #Creating a new activity
        new_activity = Friends.objects.create(name='mhs_name',url='https://google.com')

        #Retrieving all available activity
        counting_all_available_friend = Friends.objects.all().count()
        self.assertEqual(counting_all_available_friend,1)

    def test_form_validation_for_blank_items(self):
        form = Friend_Form(data={'name': '', 'url': ''})
        self.assertFalse(form.is_valid())

    def test_post_fail(self):
        response = Client().post('/friend/add_friend', {'name': 'Anonymous', 'url':''})
        self.assertEqual(response.status_code, 302)
        response = Client().post('/friend/add_friend', {'name': 'Anonymous', 'url':'https://l.herokuapp.com'})
        self.assertEqual(response.status_code, 302)
        html_response = response.content.decode('utf8')

    #@unittest.skip
    def test_post_success_and_render_the_result(self):
        anonymous = 'Anonymous'
        url = 'https://lab1-ppw.herokuapp.com'
        post = Client().post('/friend/add_friend/', {'name': anonymous, 'url' : url})
        self.assertEqual(post.status_code, 302)
        response = Client().get('/friend/')
        html_response = response.content.decode('utf8')
        self.assertIn(anonymous,html_response)
        self.assertIn(url,html_response)

    #@unittest.skip
    def test_showing_all_friends(self):

        name = 'Budi'
        url = 'https://lab1-ppw.herokuapp.com'
        post_data1 = Client().post('/friend/add_friend', {'name': name, 'url': url})
        self.assertEqual(post_data1.status_code, 302)

        name = 'Ryo'
        url = 'https://lab1-ppw.herokuapp.com'
        post_data2 = Client().post('/friend/add_friend/', {'name': name, 'url': url})
        self.assertEqual(post_data2.status_code, 302)

        response = Client().get('/friend/')
        html_response = response.content.decode('utf8')

        self.assertIn('Budi', html_response)
        self.assertIn(url, html_response)
        self.assertIn(name, html_response)

    def test_copyright_navbar(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Status',html_response)
        self.assertIn('Friends',html_response)
        self.assertIn('Stats',html_response)
        self.assertIn('Profile',html_response)
