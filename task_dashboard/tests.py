from django.test import TestCase,Client 
from django.urls import resolve
from .views import index, response
from task_update.models import Status
from task_friend.models import Friends
from django.utils import timezone
from django.http import HttpRequest

# Create your tests here.
class StatisticTest(TestCase):

    def test_dashboard_url_is_exist(self):
       response = Client().get('/dashboard/')
       self.assertEqual(response.status_code, 200)

    def test_dashboard_using_index_func(self):
       found = resolve('/dashboard/')
       self.assertEqual(found.func, index)

    def test_navbar_and_footer_is_exist(self):
       request = HttpRequest()
       response = index(request)
       html_response = response.content.decode('utf8')
       self.assertIn('navbar', html_response)
       self.assertIn('Created by',html_response)

    def test_status_count_is_correct(self):
       Status.objects.create(status='Test')
       jumlahstatus = Status.objects.all().count();
       self.assertEqual(jumlahstatus, 1)

    def test_latest_post_is_correct(self):
        Status.objects.create(status='Test')
        Status.objects.create(status='Test2')
        latest_post = Status.objects.last();
        self.assertEqual(latest_post.status, 'Test2')

    def test_friend_count_is_correct(self):
       friends = Friends.objects.create(name='friend', url='https://ppw-lab.herokuapp.com')
       jumlahteman = Friends.objects.all().count()
       self.assertEqual(jumlahteman, 1)