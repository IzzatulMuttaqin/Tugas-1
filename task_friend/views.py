from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Friend_Form
from .models import Friends
from django.contrib import messages
import urllib

response = {}
def index(request):
    html = 'task_friend/task_friend.html'
    response['author'] = 'Izzatul Muttaqin'
    response['title'] = 'Friend'
    response['form'] = Friend_Form
    friends = Friends.objects.all()
    response['friend'] = friends

    return render(request, html, response)

def add_friend(request):
    form = Friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid() and '.herokuapp.com' in request.POST['url']):
        try:
            code = urllib.request.urlopen(request.POST['url']).getcode()
            response['name'] = request.POST['name']
            response['url'] = request.POST['url']
            friend = Friends(name=response['name'], url=response['url'])
            friend.save()
            return HttpResponseRedirect('/friend/')
        except:
            messages.warning(request, 'Please insert a valid herokuapp.')
            return HttpResponseRedirect('/friend/')
    else:
        messages.warning(request, 'Please input herokuapp site.')
        return HttpResponseRedirect('/friend/')