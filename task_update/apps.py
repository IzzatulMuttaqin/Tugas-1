from django.apps import AppConfig


class TaskUpdateConfig(AppConfig):
    name = 'task_update'
