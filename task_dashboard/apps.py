from django.apps import AppConfig


class TugasDashboardConfig(AppConfig):
    name = 'tugas_dashboard'
