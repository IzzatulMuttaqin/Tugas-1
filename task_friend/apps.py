from django.apps import AppConfig


class TaskFriendConfig(AppConfig):
    name = 'task_friend'
