## Coverage
[![coverage report](https://gitlab.com/IzzatulMuttaqin/Tugas-1/badges/master/coverage.svg)](https://gitlab.com/IzzatulMuttaqin/Tugas-1/commits/master)

## Pipeline
[![pipeline status](https://gitlab.com/IzzatulMuttaqin/Tugas-1/badges/master/pipeline.svg)](https://gitlab.com/IzzatulMuttaqin/Tugas-1/commits/master)

## [HerokuApp](http://team-tugas.herokuapp.com)

# Anggota
1. [Fannyah Dita Cahya](https://gitlab.com/fannyahdita)
2. [Izzatul Muttaqin](https://gitlab.com/IzzatulMuttaqin/)
3. [Rachel Sausan Nabila](https://gitlab.com/rachelsausan)
4. [Windi Chandra](https://gitlab.com/codebie)