"""Tugas_1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import RedirectView
import task_profile.urls as task_profile
import task_update.urls as task_update
import task_dashboard.urls as tugas_dashboard
import task_friend.urls as task_friend

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^profile/', include(task_profile, namespace='profile')),
    url(r'^update_status/', include(task_update, namespace='update_status')),
    url(r'^dashboard/', include(tugas_dashboard, namespace='dashboard')),
    url(r'^friend/', include(task_friend, namespace='friend')),
    url(r'^$', RedirectView.as_view(permanent = True, url='/update_status/'), name='index'),
]