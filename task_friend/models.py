from django.db import models

class Friends(models.Model):
	name = models.CharField(max_length=30)
	url = models.URLField(max_length=200)
	date = models.DateTimeField(auto_now_add=True)
