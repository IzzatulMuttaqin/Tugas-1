from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Please, fill this',
    }
    status_attrs = {
        'type': 'text',
        'cols': 150,
        'rows': 4,
        'class': 'status-form-textarea',
        'placeholder':'What do u feel, now?'
    }

    status = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=status_attrs))
