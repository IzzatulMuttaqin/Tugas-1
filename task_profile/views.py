from django.shortcuts import render
from datetime import datetime, date
from .models import Profile, Expertise

# Create your views here.
u_name = 'Ariana Grande'
gender ='Female'
desc = 'PPW is my favorite subject no matter how hard TDD is. *currently crying*'
email = 'holla@gmail.com'
birthdate = date(1995, 10, 31)
birth_date = birthdate.strftime('%d %B')
exp = ['Django', 'Tapi Boong']

#bio_dict = [{'subject' : 'Birthday', 'value' : birth_date},\
#{'subject' : 'Gender', 'value' : gender}, \
#{'subject' : 'Description', 'value' : desc}, {'subject' : 'Email', 'value' : email}]

def index(request):
	profile = Profile(u_name=u_name, gender=gender, desc=desc, email=email)
	expert = Expertise(exp=exp)
	response = {'u_name' : profile.u_name, 'birth_date' : birth_date, \
	'gender':profile.gender, 'desc':profile.desc, 'email':profile.email, 'exp' : expert.exp}
	response['author'] = 'Fannyah Dita'
	response['title'] = 'Profile'
	html = 'tugas_profile/tugas_profile.html'
	return render(request, html, response)