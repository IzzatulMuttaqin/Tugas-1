from django import forms

class Friend_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan URL',
    }
    name_attrs = {
        'class': 'form-control',
        'placeholder': "Your friend's name",
    }
    url_attrs = {
        'class': 'form-control',
        'placeholder': "Your friend's herokuapp",
    }

    name = forms.CharField(label="Name",required=True, max_length=27, widget=forms.TextInput(attrs=name_attrs))
    url = forms.URLField(label="Url",required=True, widget=forms.URLInput(attrs=url_attrs))
