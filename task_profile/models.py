from django.db import models

# Create your models here.

class Profile(models.Model):
	u_name = models.CharField(max_length=50)
	gender = models.CharField(max_length=6)
	desc = models.TextField(max_length=350)
	email = models.EmailField()

class Expertise(models.Model):
	exp = models.TextField(max_length=100)